export const changeUsersAge = (age) => {
    return {
        type: 'CHANGEUSERAGE',
        payload: age,
    };
};