const initialState = {name: 'User 1', age: 26};
const userReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'CHANGEUSERNAME':
            return {...state, name: action.payload};
        case 'CHANGEUSERAGE':
            return {...state, age: action.payload};
    }
    return state;
};

export default userReducer;