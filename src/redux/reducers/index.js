import {combineReducers} from "redux";
import userReducer from "./addUserReducer";

const allReducers = combineReducers({
    userReducer: userReducer,
});

export default allReducers;